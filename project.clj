(defproject game-of-life "0.2.0"
  :description "Conway's Game of Life in Clojurescript"
  :url "https://pseudonamed.github.io/game-of-life-site/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}


  :min-lein-version "2.7.1"

  :jvm-opts ["--add-modules" "java.xml.bind"]

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.9.946"]
                 [org.clojure/math.combinatorics "0.1.4"]
                 [quil "2.6.0"]
                 [reagent "0.7.0"]
                 [re-frame "0.10.5"]

                 [cljsjs/react "15.6.1-1"]
                 [cljsjs/react-dom "15.6.1-1"]

                 [devcards "0.2.4"]
                 [lein-doo "0.1.8"]]

  :plugins [[lein-figwheel "0.5.14"]
            [lein-doo "0.1.8"]
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]

  :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                    "target"]
  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id "devcards"
                :source-paths ["src"]
                :figwheel {:devcards true
                           :open-urls ["http://localhost:3449/cards.html"]}
                :compiler {:main game-of-life.cards
                           :asset-path "js/compiled/devcards_out"
                           :output-to "resources/public/js/compiled/main_devcards.js"
                           :output-dir "resources/public/js/compiled/devcards_out"
                           :source-map-timestamp true }}
               {:id "dev"
                :source-paths ["src"]
                :figwheel {:open-urls ["http://localhost:3449/index.html"]}
                :compiler {:main game-of-life.ui
                           :asset-path "js/compiled/out"
                           :output-to "resources/public/js/compiled/main.js"
                           :output-dir "resources/public/js/compiled/out"
                           :source-map-timestamp true }}
               {:id "prod"
                :source-paths ["src"]
                :compiler {:main game-of-life.renderer
                           :asset-path "js/compiled/out"
                           :output-to "resources/public/js/compiled/main.js"
                           :optimizations :advanced}}
               {:id "test"
                :source-paths ["src" "test"]
                :compiler {:main runners.doo
                           :optimizations :none
                           :output-dir "resources/public/js/compiled/test_out"
                           :output-to "resources/public/js/compiled/tests.js"}}
               {:id "devcards-test"
                :source-paths ["src" "test"]
                :figwheel {:devcards true
                           :open-urls ["http://localhost:3449/tests.html"]}
                :compiler {:main runners.browser
                           :optimizations :none
                           :asset-path "js/compiled/devcards_test_out"
                           :output-dir "resources/public/js/compiled/devcards_test_out"
                           :output-to "resources/public/js/compiled/devcards_tests.js"
                           :source-map-timestamp true}}]}

  :figwheel {:css-dirs ["resources/public/css"]}

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.2"]
                                  [figwheel-sidecar "0.5.14"]
                                  [com.cemerick/piggieback "0.2.1"]
                                  [day8.re-frame/re-frame-10x "0.3.0"]
                                  [day8.re-frame/tracing "0.5.0"]]
                   :source-paths ["src" "dev"]
                   ;; for CIDER
                   ;; :plugins [[cider/cider-nrepl "0.12.0"]]
                   :repl-options {:init (set! *print-length* 50)
                                  :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}})
