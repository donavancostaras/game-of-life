(ns game-of-life.cards
  (:require
   [devcards.core :refer-macros [defcard-doc defcard defcard-rg]]
   [game-of-life.state :as state]
   [game-of-life.ui :as ui]))

(defcard-doc
  "## Loading GOL patterns")

(defcard-rg block-size
  "Block size input"
  [ui/block-size]
  state/state)

(defcard-rg canvas-size
  "Canvas size input"
  [ui/canvas-size]
  state/state)

(defcard-rg pattern-loader
  "Pattern loader"
  [ui/pattern-loader]
  state/state)

(defcard-rg start-stop-button
  "Start stop button"
  [:div
   [ui/start-world]
   [ui/stop-world]]
  state/state)

(defcard-rg game-canvas
  "game canvas"
  [ui/game-canvas]
  state/state)
