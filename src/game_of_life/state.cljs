(ns game-of-life.state
  (:require [game-of-life.core :as gol]
            [reagent.core :as reagent]
            [quil.core :as q :include-macros true]
            [clojure.set :refer [intersection difference union]]))

(defonce state (reagent/atom {:world #{}
                              :renderer {:running false
                                         :canvas-id "game-of-life_canvas"
                                         :block-size 5
                                         :canvas-size 500}}))

(defn redraw
  []
  (let [sketch (q/get-sketch-by-id (:canvas-id (:renderer @state)))]
    (q/with-sketch sketch
      (q/redraw))))

(defn update-world!
  [f & args]
  (apply swap! state update-in [:world] f args))

(defn add-to-world!
  [cells]
  (update-world! union cells)
  (redraw))

(defn stop-game
  []
  (swap! state assoc-in [:renderer :running] false))

(defn start-game
  []
  (swap! state assoc-in [:renderer :running] true))

(defn get-world
  []
  (let [this-generation (:world @state)
        next-generation (gol/next-generation this-generation)]
    (swap! state assoc-in [:world] next-generation)
    this-generation))

(defn update-block-size
  [size]
  (swap! state assoc-in [:renderer :block-size] size)
  (redraw))

(defn update-canvas-size
  [size]
  (swap! state assoc-in [:renderer :canvas-size] size)
  (redraw))

(def canvas-id (reagent/cursor state [:renderer :canvas-id]))
(def renderer-running (reagent/cursor state [:renderer :running]))
(def block-size (reagent/cursor state [:renderer :block-size]))
(def canvas-size (reagent/cursor state [:renderer :canvas-size]))
