(ns game-of-life.ui
  (:require [reagent.core :as reagent]
            [clojure.set :refer [union]]
            [game-of-life.patterns :as pat]
            [game-of-life.renderer :as renderer]
            [game-of-life.core :as gol]
            [game-of-life.state :as state]))

(enable-console-print!)

(defn load-pattern-button
  [name pattern]
  [:button
   {:on-click (fn [] (state/add-to-world! pattern))}
   (str "Load " name)])

(defn pattern-loader
  []
  [:div
   (doall
    (for [[k v :as pattern] pat/all-forms]
      ^{:key (name k)} [load-pattern-button (name k) v]))])

(defn stop-world
  []
  [:button
   {:on-click (fn [] (state/stop-game))}
   "stop"])

(defn start-world
  []
  [:button
   {:on-click (fn [] (state/start-game))}
   "start"])

(defn block-size
  []
  [:input
   {:type "text"
    :value @state/block-size
    :on-change (fn [e]
                 (state/update-block-size
                  (-> e .-target .-value)))}])

(defn canvas-size
  []
  [:input
   {:type "text"
    :value @state/canvas-size
    :on-change (fn [e]
                 (state/update-canvas-size
                  (-> e .-target .-value)))}])

(defn game-canvas
  []
  (let [canvas-class @state/canvas-id]
    (reagent/create-class
     {:component-did-mount
      (fn [this]
        (renderer/create-sketch canvas-class))
      :reagent-render
      (fn []
        (if @state/renderer-running
          (renderer/start-world canvas-class)
          (renderer/stop-world canvas-class))
        [:div
         [:canvas {:id canvas-class}]])})))
