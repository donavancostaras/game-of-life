(ns game-of-life.renderer
  (:require [quil.core :as q :include-macros true]
            [quil.middleware :as m]
            [clojure.set :refer [union]]
            [game-of-life.core :as gol]
            [game-of-life.state :as state]
            [game-of-life.patterns :as patterns]))

(enable-console-print!)

(defn on-js-reload [])

(defn setup
  []
  (q/frame-rate 20)
  (q/color-mode :hsb)
  (q/background 240)
  {:generation (state/get-world)
   :canvas-size @state/canvas-size
   :block-size @state/block-size})

(defn draw-state
  []
  (let [canvas-size @state/canvas-size
        block-size @state/block-size
        generation (state/get-world)]
    (do
      (q/background 240)
      (q/fill 255 0 0)
      (let [center-offset (- (/ canvas-size 2) (/ block-size 2))]
        (doseq [[x y] generation]
          (q/rect
           (+ center-offset (* block-size x))
           (+ center-offset (* block-size y))
           block-size
           block-size))))))

(defn redraw
  [sketch-id]
  (let [sketch (q/get-sketch-by-id sketch-id)]
    (q/with-sketch sketch
      (q/redraw))))

(defn start-world
  [sketch-id]
  (let [sketch (q/get-sketch-by-id sketch-id)]
    (q/with-sketch sketch
      (q/start-loop))))

(defn stop-world
  [sketch-id]
  (if-let [sketch (q/get-sketch-by-id sketch-id)]
    (q/with-sketch sketch
      (q/no-loop))))

(defn create-sketch
  [canvas-id]
  (q/sketch
   :host canvas-id
   :size [500 500]
   :setup setup
   :draw draw-state)
  (stop-world canvas-id))
