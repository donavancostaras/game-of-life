resources-make = $(MAKE) -C ./resources/public
deploy = $(resources-make) github-pages

all:; lein figwheel dev devcards devcards-test

dev:; lein figwheel dev

test:; lein figwheel devcards-test

cards:; lein figwheel devcards

headless-test:; lein doo phantom test once

deploy-full: advanced-build; $(deploy)

deploy-files:; $(deploy)

advanced-build:; lein do clean, cljsbuild once prod

clean:
	rm -f *-init.clj figwheel_server.log
	lein clean

.PHONY:
	advanced-build \
	clean \
	deploy-files \
	deploy-full \
	headless-test \
	cards \
	test \
	dev \
	all
